FROM ethereum/client-go
RUN apk add --no-cache git bash mercurial && \ 
    cd /root/ && \ 
    mkdir ./eth && \ 
    ls -l

ADD node.password /root/eth/node.password

ADD run.sh /root/run.sh
RUN chmod +x /root/run.sh

EXPOSE 8545 30311 30311/udp 

ENTRYPOINT /root/run.sh