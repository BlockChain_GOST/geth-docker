#!/bin/bash

ETH_ROOT=/root/eth 
ETH_DATA_DIR=$ETH_ROOT/data 
SPEC_DIR=$ETH_ROOT/testnet-chain-spec

if [ ! -d $SPEC_DIR ]; then
    cd $ETH_ROOT && git clone https://gitlab.com/BlockChain_GOST/testnet-chain-spec.git

     echo "Running 'geth account new'"
     geth --networkid 13 --datadir $ETH_DATA_DIR --password $ETH_ROOT/node.password account new

fi

BOOTNODE_ADDRESS=$(< $SPEC_DIR/bootnodes.txt) 

echo "Init Geth account"
geth --datadir=$ETH_DATA_DIR init "$SPEC_DIR/genesis.json"

sleep 3

echo "Bootnodes:"
echo $BOOTNODE_ADDRESS

geth --bootnodes="$BOOTNODE_ADDRESS" $@
geth \
    --bootnodes="$BOOTNODE_ADDRESS" \
    --datadir=$ETH_DATA_DIR \
    --port=30311 \
    --gasprice='1' \
    --rpc \
    --rpcaddr='0.0.0.0' \
    --rpccorsdomain='*' \
    --rpcapi admin,net,eth,web3,miner,personal \
    --rpcport=8545 \
    --password "$ETH_ROOT/node.password" \
    --networkid=13 \
    --nodiscover \
    --verbosity=4 \
    --unlock 0